import Data.List.Split

readNums = parse <$> readFile "input"

splitChunks = splitWhen (=="") . (splitWhen (=='\n'))
parse = (map . map) (read :: String -> Integer) . splitChunks

prog1 :: [[Integer]] -> Integer
prog1 nums = maximum $ sum <$> nums
main1 = print =<< prog1 <$> readNums

(.:.) = (.)(.)(.)  -- g.:.f $ x y = g $ f x y
insert (x:xs) y = min x y : insert xs (max x y)
insert [] y = [y]

prog2 :: [[Integer]] -> Integer
prog2 nums = sum $ foldl (tail .:. insert) [0,0,0] $ sum <$> nums
main2 = print =<< prog2 <$> readNums
